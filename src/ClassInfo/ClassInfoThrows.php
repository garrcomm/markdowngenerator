<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\ClassInfo;

/**
 * ClassInfoThrows object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoThrows
{
    /**
     * Fully qualified classname of the Throwable class
     *
     * @var string
     */
    private string $throwableFQCN;
    /**
     * Description of when it's thrown
     *
     * @var string
     */
    private string $description;

    /**
     * Constructs a new Throws definition
     *
     * @param string $throwableFQCN Fully qualified classname of the Throwable class.
     * @param string $description   Description of when it's thrown.
     */
    public function __construct(string $throwableFQCN, string $description)
    {
        $this->throwableFQCN = $throwableFQCN;
        $this->description = $description;
    }

    /**
     * Returns the fully qualified classname of the Throwable class
     *
     * @return string
     */
    public function getThrowableFQCN(): string
    {
        return $this->throwableFQCN;
    }

    /**
     * Returns the description of when it's thrown
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
