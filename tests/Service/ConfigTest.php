<?php

declare(strict_types=1);

namespace Tests\Service;

use Garrcomm\MarkdownGenerator\Service\Config;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use InvalidArgumentException;

/**
 * Config unit tests
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ConfigTest extends TestCase
{
    /**
     * Returns test data for testReadConfig()
     *
     * @return array<string, array<string, string|null>>
     */
    public function readConfigDataProvider(): array
    {
        return [
            'Basic config' => [
                'configFile' => __DIR__ . '/../TestSubject/mdgen.json',
                'summaryFile' => null,
                'absoluteSummaryFile' => null,
                'outputRelativeToSummary' => null,
                'baseNamespace' => '\\',
            ],
            'Without bootstrap' => [
                'configFile' => __DIR__ . '/../TestSubject/mdgen-no-bootstrap.json',
                'summaryFile' => null,
                'absoluteSummaryFile' => null,
                'outputRelativeToSummary' => null,
                'baseNamespace' => '\\',
            ],
            'With summary' => [
                'configFile' => __DIR__ . '/../TestSubject/mdgen-summary.json',
                'summaryFile' => 'summary.md',
                'absoluteSummaryFile' => realpath(__DIR__ . '/../') . '/TestSubject/summary.md',
                'outputRelativeToSummary' => '',
                'baseNamespace' => 'Tests\TestSubject\\',
            ],
            'With summary in a path above the output' => [
                'configFile' => __DIR__ . '/../TestSubject/mdgen-summary-forward-path.json',
                'summaryFile' => '../summary.md',
                'absoluteSummaryFile' => realpath(__DIR__ . '/../') . '/summary.md',
                'outputRelativeToSummary' => 'TestSubject/',
                'baseNamespace' => 'Tests\TestSubject\\',
            ],
            'With summary in a path within the output' => [
                'configFile' => __DIR__ . '/../TestSubject/mdgen-summary-backward-path.json',
                'summaryFile' => './docs/summary.md',
                'absoluteSummaryFile' => realpath(__DIR__ . '/../') . '/TestSubject/docs/summary.md',
                'outputRelativeToSummary' => '../',
                'baseNamespace' => 'Tests\TestSubject\\',
            ],
        ];
    }

    /**
     * Tests a completely valid configuration file
     *
     * @param string      $configFile              Path to the configuration file.
     * @param string|null $summaryFile             Relative path to the summary file.
     * @param string|null $absoluteSummaryFile     Absolute path to the summary file.
     * @param string|null $outputRelativeToSummary Relative path between output and the summary file.
     * @param string      $baseNamespace           Base namespace name.
     *
     * @return void
     *
     * @dataProvider readConfigDataProvider
     */
    public function testReadConfig(
        string $configFile,
        ?string $summaryFile,
        ?string $absoluteSummaryFile,
        ?string $outputRelativeToSummary,
        string $baseNamespace
    ): void {
        $outputDir = realpath(__DIR__ . '/../TestSubject');
        if ($outputDir === false) {
            throw new RuntimeException('Can\'t determine the output directory');
        }

        $config = new Config($configFile);
        $this->assertEquals($outputDir . '/', $config->getOutputDirectory());
        $this->assertEquals($summaryFile, $config->getSummaryFile());
        $this->assertEquals($absoluteSummaryFile, $config->getSummaryFile(true));
        $this->assertEquals($baseNamespace, $config->getBaseNamespace());
        $this->assertEquals($outputRelativeToSummary, $config->getOutputRelativeToSummary());
        $this->assertEquals(
            [
                'Garrcomm\\MarkdownGenerator\\ClassInfo\\ClassInfo',
                'Garrcomm\\MarkdownGenerator\\ClassInfo\\ClassInfoConstant',
                'Garrcomm\\MarkdownGenerator\\ClassInfo\\ClassInfoMethod',
                'Garrcomm\\MarkdownGenerator\\ClassInfo\\ClassInfoParameter',
                'Garrcomm\\MarkdownGenerator\\ClassInfo\\ClassInfoReturnType',
                'Garrcomm\\MarkdownGenerator\\ClassInfo\\ClassInfoThrows',
                'Garrcomm\\MarkdownGenerator\\Controller\\GeneratorController',
                'Garrcomm\\MarkdownGenerator\\Service\\Config',
                'Garrcomm\\MarkdownGenerator\\Service\\MarkdownGenerator',
                'Tests\\ClassInfo\\ClassInfoReturnTypeTest',
                'Tests\\ClassInfo\\ClassInfoTest',
                'Tests\\Service\\ConfigTest',
                'Tests\\Service\\MarkdownGeneratorTest',
            ],
            $config->getObjects()
        );
    }

    /**
     * Tests the error of no output dir specified
     *
     * @return void
     */
    public function testNoOutputDir(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Config value "outputdir" is not a string');
        new Config(__DIR__ . '/../TestSubject/mdgen-no-output-dir.json');
    }

    /**
     * Tests the error of a non-existent output dir
     *
     * @return void
     */
    public function testInvalidOutputDir(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessageMatches('/^Can\'t find the output directory\:/');
        new Config(__DIR__ . '/../TestSubject/mdgen-invalid-output-dir.json');
    }

    /**
     * Tests the error of a non-existent config file
     *
     * @return void
     */
    public function testNonExistentConfigFile(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Config file doesn\'t exist');
        new Config(__DIR__ . '/../TestSubject/mdgen-doesnt-exist.json');
    }

    /**
     * Tests the error of an invalid json format
     *
     * @return void
     */
    public function testInvalidJson(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessageMatches('/^Can\'t parse config file\:/');
        new Config(__DIR__ . '/../TestSubject/mdgen-invalid-json.json');
    }

    /**
     * Tests the error of an invalid json format (no array)
     *
     * @return void
     */
    public function testInvalidJsonNoArray(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Config file should contain a json array');
        new Config(__DIR__ . '/../TestSubject/mdgen-invalid-json-2.json');
    }

    /**
     * Tests the error of an invalid bootstrap file
     *
     * @return void
     */
    public function testInvalidBootstrap(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Bootstrap file doesn\'t exist');
        new Config(__DIR__ . '/../TestSubject/mdgen-invalid-bootstrap.json');
    }

    /**
     * Tests the error of a broken bootstrap file
     *
     * @return void
     */
    public function testBrokenBootstrap(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessageMatches('/^Can\'t load the bootstrap file\:/');
        new Config(__DIR__ . '/../TestSubject/mdgen-broken-bootstrap.json');
    }

    /**
     * Tests the error of a broken bootstrap file
     *
     * @return void
     */
    public function testBrokenClass(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessageMatches('/^Can\'t load class file/');
        new Config(__DIR__ . '/../TestSubject/mdgen-broken-class.json');
    }

    /**
     * Tests the error of a non-matching input file
     *
     * @return void
     */
    public function testFailedInputFile(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No class files found');
        new Config(__DIR__ . '/../TestSubject/mdgen-invalid-input-file.json');
    }
    /**
     * Tests the error of a no input file specified
     *
     * @return void
     */
    public function testNoInputFile(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No inputfiles specified');
        new Config(__DIR__ . '/../TestSubject/mdgen-no-input-files.json');
    }
}
