# Markdown Generator

For my code projects, I like to provide proper documentation.
Best way, in my opinion, is to start with well-documented code.

To generate markdown formatted documentation based on the codebase, I wrote this small tool.

It uses PHP Reflection to read out code information and formats according to the structure used on [php.net](https://www.php.net/manual/en/) documentation.

These projects can be used as an example:

| Library                       | Markdown file                                                         | Config                                                                                    |
|-------------------------------|-----------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| garrcomm/uuid                 | [README.md](https://bitbucket.org/garrcomm/php-uuid/src/v1/README.md) | [.mdgen.json](https://bitbucket.org/garrcomm/php-uuid/src/v1/.mdgen.json)                 |
| garrcomm/networking-utilities | [README.md](https://bitbucket.org/garrcomm/php-uuid/src/v1/README.md) | [.mdgen.json](https://bitbucket.org/garrcomm/php-networking-utilities/src/v1/.mdgen.json) |

## How to use

Usage is very simple;

1. Load this package by composer with  
   ```shell
   composer require garrcomm/markdown-generator v1.x-dev --dev
   ```
2. Create a config file (see the list above for examples)
3. Execute the generator;
   ```shell
   vendor/bin/mdgen mdgen.json
   ```
