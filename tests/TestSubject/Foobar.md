## Class examples

This is a PHP example:

```php
echo Foobar::whoami();
```

Something entirely different, but this is how to list files on linux:

```
ls -la .
```

And another PHP example
with two lines of introduction.

```php
echo (new Foobar())->baz('foo');
```

## Class synopsis

```php
class Foobar {

    /* Constants */
    public const FOOBAR = 'baz';

    /* Methods */
    public __construct(string foo, int bar = 1, mixed baz = 'foobar')
    public anotherOne(?string value): string
    public baz(string param1): string
    public longParamDescriptions(string param1, string param2, string param3): null
    public yetAnotherOne(): string
    public static whoami(): string
}
```

<a id="constants"></a>
## Constants

**Foobar::FOOBAR**  
&nbsp;&nbsp;&nbsp;&nbsp;Public constant value


<a id="methods"></a>
## Methods

* [Foobar::__construct](#foobar----construct) — 
* [Foobar::anotherOne](#foobar--anotherone) — Also just returns the input value
* [Foobar::baz](#foobar--baz) — Returns the literal string back
* [Foobar::longParamDescriptions](#foobar--longparamdescriptions) — Sometimes a parameter can have a long description.
    
    Also, methods can have a longer description.
    
* [Foobar::yetAnotherOne](#foobar--yetanotherone) — Different notation
* [Foobar::whoami](#foobar--whoami) — Returns the name of the called class


<a id="foobar----construct"></a>
## Foobar::__construct
#### Description

```php
public Foobar::__construct(string foo, int bar = 1, mixed baz = 'foobar')
```



#### Parameters

* **foo**  

* **bar**  

* **baz**  


***
<a id="foobar--anotherone"></a>
## Foobar::anotherOne
#### Description

```php
public Foobar::anotherOne(?string value): string
```

Also just returns the input value

#### Parameters

* **value**  
    Input value.

#### Return Values

A string is returned when a string is given.
Null is returned when null is given.

***
<a id="foobar--baz"></a>
## Foobar::baz
#### Description

```php
public Foobar::baz(string param1): string
```

Returns the literal string back

#### Parameters

* **param1**  
    Input string.

#### Return Values



#### Errors/Exceptions

Thrown when the input string is empty.

***
<a id="foobar--longparamdescriptions"></a>
## Foobar::longParamDescriptions
#### Description

```php
public Foobar::longParamDescriptions(string param1, string param2, string param3): null
```

Sometimes a parameter can have a long description.
 
 Also, methods can have a longer description.

#### Parameters

* **param1**  
    Short one.
* **param2**  
    This parameter has a longer description that is placed onto multiple lines which we also  
    should support. Normally, whitespace is ignored, so for a next line we can use <br>  
    So this content should end up on the next line.
* **param3**  
    Short three.

#### Return Values



***
<a id="foobar--yetanotherone"></a>
## Foobar::yetAnotherOne
#### Description

```php
public Foobar::yetAnotherOne(): string
```

Different notation

#### Return Values



***
<a id="foobar--whoami"></a>
## Foobar::whoami
#### Description

```php
public static Foobar::whoami(): string
```

Returns the name of the called class

#### Return Values



