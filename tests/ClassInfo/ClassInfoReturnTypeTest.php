<?php

declare(strict_types=1);

namespace Tests\ClassInfo;

use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfoReturnType;
use PHPUnit\Framework\TestCase;

/**
 * ClassInfoReturnType unit tests
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoReturnTypeTest extends TestCase
{
    /**
     * Returns a list of tests for the constructor
     *
     * @return array<int, array{inputType: string, description: string, outputType: string, nullable: bool}>
     */
    public function constructorDataProvider(): array
    {
        return [
            [
                'inputType'   => 'string',
                'description' => 'Actual string',
                'outputType'  => 'string',
                'nullable'    => false,
            ],
            [
                'inputType'   => '?string',
                'description' => 'Actual string',
                'outputType'  => 'string',
                'nullable'    => true,
            ],
            [
                'inputType'   => 'int|null',
                'description' => 'An integer',
                'outputType'  => 'int',
                'nullable'    => true,
            ],
        ];
    }

    /**
     * Executes tests on the constructor
     *
     * @param string  $inputType   The input type field.
     * @param string  $description The description.
     * @param string  $outputType  The type outputted.
     * @param boolean $nullable    Whether it's nullable.
     *
     * @return void
     *
     * @dataProvider constructorDataProvider
     */
    public function testConstructor(string $inputType, string $description, string $outputType, bool $nullable): void
    {
        $info = new ClassInfoReturnType($inputType, $description);
        $this->assertEquals($outputType, $info->getType());
        $this->assertEquals($description, $info->getDescription());
        $this->assertEquals($nullable, $info->isNullable());
    }
}
