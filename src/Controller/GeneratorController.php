<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\Controller;

use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfo;
use Garrcomm\MarkdownGenerator\Service\Config;
use Garrcomm\MarkdownGenerator\Service\MarkdownGenerator;
use Throwable;
use InvalidArgumentException;
use RuntimeException;

/**
 * GeneratorController object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class GeneratorController
{
    /**
     * Executes the generator and returns the exit code
     *
     * @param string[] $arguments Command line arguments.
     *
     * @return integer
     */
    public function run(array $arguments): int
    {
        $cmd = array_shift($arguments); // Remove the call to the script itself
        if (empty($arguments)) {
            echo 'Usage:' . PHP_EOL;
            echo '  ' . $cmd . ' [configfile]' . PHP_EOL;
            echo PHP_EOL;
            return 1;
        }

        echo 'Reading configuration...' . PHP_EOL;
        try {
            $config = new Config($arguments[0]);
        } catch (InvalidArgumentException | RuntimeException $exception) {
            echo 'Error while reading configuration: ' . $exception->getMessage() . PHP_EOL;
            return 1;
        } catch (Throwable $throwable) {
            echo 'An unexpected error occured: ' . $throwable->getMessage() . PHP_EOL;
            return 1;
        }
        try {
            $summary = '# Summary' . PHP_EOL . PHP_EOL;
            foreach ($config->getObjects() as $object) {
                $filename = $config->getOutputDirectory() . $this->classNameToFilename($object);
                echo 'Generating ' . $filename . '...' . PHP_EOL;
                $generator = new MarkdownGenerator(new ClassInfo($object));
                $data = '# ' . $this->relativeClassName($object, $config->getBaseNamespace()) . PHP_EOL . PHP_EOL;
                $data .= $generator->getClassExamples();
                $data .= $generator->getClassSynopsis();
                $data .= $generator->getConstants();
                $data .= $generator->getMethodList();
                $data .= $generator->getMethodDetails();
                file_put_contents($filename, $data);
                if ($config->getSummaryFile() !== null) {
                    $relativeName = $this->relativeClassName($object, $config->getBaseNamespace());
                    $summary .= '<a id="' . $generator->getAnchorName($relativeName) . '"></a>' . PHP_EOL;
                    $summary .= '## ' . $relativeName . PHP_EOL . PHP_EOL;
                    $summary .= $generator->getClassExamples(3);
                    $summary .= $generator->getClassSynopsis(3);
                    $summary .= $generator->getConstants(3);
                    $summary .= $generator->getMethodList(
                        $config->getOutputRelativeToSummary() . $this->classNameToFilename($object) . '#',
                        3
                    );
                }
            }
            if ($config->getSummaryFile() !== null) {
                echo 'Generating ' . $config->getSummaryFile(true) . '...' . PHP_EOL;
                file_put_contents($config->getSummaryFile(), $summary);
            }
        } catch (Throwable $throwable) {
            echo 'An unexpected error occured: ' . $throwable->getMessage() . PHP_EOL;
            return 1;
        }
        echo 'Done!' . PHP_EOL;

        return 0;
    }

    /**
     * Determines the filename based on a fully qualified classname
     *
     * @param string $classFQN The fully qualified classname.
     *
     * @return string
     */
    private function classNameToFilename(string $classFQN): string
    {
        return str_replace('\\', '__', $classFQN) . '.md';
    }

    /**
     * Returns the relative class name from a specific base namespace
     *
     * @param string $classFQN      The fully qualified class name.
     * @param string $baseNamespace The namespace.
     *
     * @return string
     */
    private function relativeClassName(string $classFQN, string $baseNamespace): string
    {
        if (substr($classFQN, 0, strlen($baseNamespace)) == $baseNamespace) {
            return substr($classFQN, strlen($baseNamespace));
        }
        return $classFQN;
    }
}
