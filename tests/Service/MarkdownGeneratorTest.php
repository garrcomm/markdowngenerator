<?php

declare(strict_types=1);

namespace Tests\Service;

use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfo;
use Garrcomm\MarkdownGenerator\Service\MarkdownGenerator;
use PHPUnit\Framework\TestCase;
use Tests\TestSubject\EmptyClass;
use Tests\TestSubject\Foobar;
use Tests\TestSubject\JsonFoobar;
use RuntimeException;

/**
 * MarkdownGenerator unit tests
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class MarkdownGeneratorTest extends TestCase
{
    /**
     * Returns a set of test data
     *
     * @return array<int, array{classFQN: string, comparison: string}>
     */
    public function generatorDataProvider(): array
    {
        $return = array();
        foreach (
            [
                Foobar::class => __DIR__ . '/../TestSubject/Foobar.md',
                JsonFoobar::class => __DIR__ . '/../TestSubject/JsonFoobar.md',
                EmptyClass::class => __DIR__ . '/../TestSubject/EmptyClass.md',
            ] as $classFQN => $filename
        ) {
            $data = file_get_contents($filename);
            if ($data === false) {
                throw new RuntimeException('Can\'t read ' . $filename);
            }
            $return[] = [
                'classFQN' => $classFQN,
                'comparison' => $data,
            ];
        }

        return $return;
    }

    /**
     * Tests if the generator generates the expected output
     *
     * @param string $classFQN   Name of the class.
     * @param string $comparison Expected output.
     *
     * @return void
     *
     * @dataProvider generatorDataProvider
     */
    public function testGenerators(string $classFQN, string $comparison): void
    {
        $generator = new MarkdownGenerator(new ClassInfo($classFQN));
        $data = $generator->getClassExamples();
        $data .= $generator->getClassSynopsis();
        $data .= $generator->getConstants();
        $data .= $generator->getMethodList();
        $data .= $generator->getMethodDetails();
        $this->assertEquals($comparison, $data);
    }
}
