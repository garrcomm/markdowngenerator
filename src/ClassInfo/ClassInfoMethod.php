<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\ClassInfo;

use ReflectionMethod;
use ReflectionType;
use ReflectionNamedType;
use InvalidArgumentException;

/**
 * ClassInfoMethod object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoMethod
{
    /**
     * Name of the method
     *
     * @var string
     */
    private string $name;
    /**
     * All parameters of the method
     *
     * @var array<string, ClassInfoParameter>
     */
    private array $parameters;
    /**
     * All throw statements in the method
     *
     * @var array<string, ClassInfoThrows>
     */
    private array $throws = [];
    /**
     * Method description
     *
     * @var string
     */
    private string $description;
    /**
     * Is it a static method?
     *
     * @var bool
     */
    private bool $static;
    /**
     * Definition of the return type(s)
     *
     * @var array<string, ClassInfoReturnType>
     */
    private array $returns;

    /**
     * Constructs a new method description
     *
     * @param ReflectionMethod $method   Reference to the reflected method.
     * @param string           $docblock The docblock as string.
     */
    public function __construct(ReflectionMethod $method, string $docblock)
    {
        $parameters = [];
        $returns = [];
        $reflectionParameters = $method->getParameters();
        foreach ($reflectionParameters as $reflectionParameter) {
            $parameters['$' . $reflectionParameter->getName()] = new ClassInfoParameter(
                $reflectionParameter->getName(),
                $this->normalizeReturnType($reflectionParameter->getType()),
                $reflectionParameter->isOptional(),
                $reflectionParameter->isOptional() ? $reflectionParameter->getDefaultValue() : null,
            );
        }
        $docblockRows = explode('@', $docblock);
        $this->description = trim(array_shift($docblockRows));
        foreach ($docblockRows as $docblockRow) {
            $docblockRow = trim($docblockRow);
            if (substr($docblockRow, 0, 6) == 'param ') {
                $parts = $this->explodeSpace($docblockRow, 4);
                if (substr($parts[1], -5) == '|null') {
                    $parts[1] = '?' . substr($parts[1], 0, -5);
                }
                //$parameters[$parts[2]]->setName($parts[2]);
                $parameters[$parts[2]]->setType($parts[1]);
                $parameters[$parts[2]]->setDescription($parts[3]);
            } elseif (substr($docblockRow, 0, 7) == 'throws ') {
                $parts = $this->explodeSpace($docblockRow, 3);
                if (isset($parts[1])) {
                    $this->throws[$parts[1]] = new ClassInfoThrows($parts[1], $parts[2] ?? '');
                }
            } elseif (substr($docblockRow, 0, 7) == 'return ') {
                $parts = $this->explodeSpace($docblockRow, 3);
                $name = $this->normalizeReturnType($parts[1]);
                $returns[$name] = new ClassInfoReturnType($name, $parts[2] ?? '');
            }
        }

        // Detects if the return type in code is also in the docblock. if not, add it to the list.
        if ($method->hasReturnType()) {
            $name = $this->normalizeReturnType($method->getReturnType());
            if (!isset($returns[$name])) {
                $returns[$name] = new ClassInfoReturnType($name, '');
            }
        }

        $this->name = $method->getName();
        $this->parameters = $parameters;
        $this->static = $method->isStatic();
        $this->returns = $returns;
    }

    /**
     * Explodes on whitespaces
     *
     * @param string  $subject String subject.
     * @param integer $limit   Max. items to return.
     *
     * @return string[]
     */
    private function explodeSpace(string $subject, int $limit): array
    {
        $parts = preg_split('/[\s]+/', $subject, $limit);
        return $parts ?: [];
    }

    /**
     * Returns the method name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns a list of all parameters
     *
     * @return array<string, ClassInfoParameter>
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Returns a list of all possible throws
     *
     * @return array<string, ClassInfoThrows>
     */
    public function getThrows(): array
    {
        return $this->throws;
    }

    /**
     * Returns the method description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Returns whether it's a static method or not
     *
     * @return boolean
     */
    public function isStatic(): bool
    {
        return $this->static;
    }

    /**
     * Returns all return types
     *
     * @return array<string, ClassInfoReturnType>
     */
    public function getReturnTypes(): array
    {
        return $this->returns;
    }

    /**
     * Formats a type consistently
     *
     * @param string|ReflectionType|null $returnType The return type.
     *
     * @return string
     *
     * @throws InvalidArgumentException Thrown when the input is of the wrong type.
     */
    private function normalizeReturnType($returnType): string
    {
        $types = [];
        if ($returnType === null) {
            $types[] = 'mixed';
        } elseif (is_object($returnType) && is_a($returnType, ReflectionNamedType::class)) {
            $types[] = $returnType->getName();
            if ($returnType->allowsNull()) {
                $types[] = 'null';
            }
        } elseif (is_string($returnType)) {
            if (substr($returnType, 0, 1) === '?') {
                $returnType = substr($returnType, 1) . '|null';
            }
            $types = explode('|', $returnType);
        } else {
            // @codeCoverageIgnoreStart
            // This should never ever happen since this is a private method. But just in case, an exception is thrown.
            throw new InvalidArgumentException('Only type null, string or ReflectionNamedType are supported');
            // @codeCoverageIgnoreEnd
        }

        // Normalize and separate null
        $typeList = array();
        $nullable = false;
        foreach ($types as $type) {
            // Store null separately
            if (strtolower($type) === 'null') {
                $nullable = true;
                continue;
            }
            // Force short notations and lower case types
            $typeList[] = str_ireplace(
                ['integer', 'boolean', 'string', 'null', 'float', 'array', 'void'],
                ['int', 'bool', 'string', 'null', 'float', 'array', 'void'],
                $type
            );
        }

        // Sort items, but null always as last item
        sort($typeList);
        return trim(implode('|', $typeList) . ($nullable ? '|null' : ''), '|');
    }
}
