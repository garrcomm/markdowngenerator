<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\ClassInfo;

/**
 * ClassInfoConstant object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoConstant
{
    /**
     * Name of the constant
     *
     * @var string
     */
    private string $name;
    /**
     * Value of the constant
     *
     * @var mixed
     */
    private $value;
    /**
     * Description of the constant
     *
     * @var string
     */
    private string $description;

    /**
     * Constructs a new class constant description
     *
     * @param string $name        Name of the constant.
     * @param mixed  $value       Value of the constant.
     * @param string $description Description of the constant.
     */
    public function __construct(string $name, $value, string $description)
    {
        $this->name = $name;
        $this->value = $value;
        $this->description = $description;
    }

    /**
     * Returns the name of the constant
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the value of the constant
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Returns the description of the constant
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
