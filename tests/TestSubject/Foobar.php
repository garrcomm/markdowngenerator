<?php

declare(strict_types=1);

namespace Tests\TestSubject;

use InvalidArgumentException;

/**
 * Foobar class
 *
 * This is a PHP example:
 * <code>
 *     echo Foobar::whoami();
 * </code>
 *
 * Something entirely different, but this is how to list files on linux:
 * <samp>
 *     ls -la .
 * </samp>
 *
 * And another PHP example
 * with two lines of introduction.
 * <code>
 *     echo (new Foobar())->baz('foo');
 * </code>
 */
class Foobar
{
    /**
     * Public constant value
     */
    public const FOOBAR = 'baz';

    /**
     * Returns the name of the called class
     */
    public static function whoami(): ?string
    {
        return get_called_class();
    }

    /**
     * This constant shouldn't be documented
     */
    private const BAZ = 'foobar';

    /**
     * Returns the literal string back
     *
     * @param string $param1 Input string.
     *
     * @return string
     *
     * @throws InvalidArgumentException Thrown when the input string is empty.
     */
    public function baz(string $param1): string
    {
        if ($param1 === '') {
            throw new InvalidArgumentException('Empty string given');
        }
        return $param1;
    }

    /**
     * This method shouldn't be documented
     *
     * @return void
     */
    private function foo(): void
    {
        // Nothing to do
    }

    public function __construct(string $foo, int $bar = 1, $baz = 'foobar')
    {
        // Nothing to do
    }

    /**
     * This method shouldn't be documented
     *
     * @return void
     */
    protected function bar(): void
    {
        // Nothing to do
    }

    /**
     * Also just returns the input value
     *
     * @param string|null $value Input value.
     *
     * @return string A string is returned when a string is given.
     * @return null   Null is returned when null is given.
     */
    public function anotherOne(?string $value)
    {
        return $value;
    }

    /**
     * Different notation
     *
     * @return ?string
     */
    public function yetAnotherOne()
    {
        return null;
    }

    /**
     * Sometimes a parameter can have a long description.
     * 
     * Also, methods can have a longer description.
     *
     * @param string $param1 Short one.
     * @param string $param2 This parameter has a longer description that is placed onto multiple lines which we also
     *                       should support. Normally, whitespace is ignored, so for a next line we can use <br>
     *                       So this content should end up on the next line.
     * @param string $param3 Short three.
     *
     * @return null
     */
    public function longParamDescriptions(string $param1, string $param2, string $param3)
    {
        return null;
    }
}
