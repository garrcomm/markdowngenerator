<?php

declare(strict_types=1);

namespace Tests\ClassInfo;

use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfo;
use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfoMethod;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;
use Tests\TestSubject\Foobar;
use Tests\TestSubject\JsonFoobar;
use JsonSerializable;

/**
 * ClassInfo unit tests
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoTest extends TestCase
{
    /**
     * Tests what happens when an invalid FQCN is given
     *
     * @return void
     */
    public function testInvalidFQCN(): void
    {
        $fqcn = 'Tests\\TestSubject\\ClassDoesNotExist';
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Not a valid FQCN: ' . $fqcn);
        new ClassInfo($fqcn);
    }

    /**
     * Tests some basic class information
     *
     * @return void
     */
    public function testFoobar(): void
    {
        // Fetches info about a specific class
        $info = new ClassInfo(Foobar::class);
        $this->assertEquals('Foobar', $info->getShortName());
        $this->assertNull($info->getExtends());
        $this->assertCount(6, $info->getMethods());
        $this->assertCount(0, $info->getImplements());
        $this->assertCount(1, $info->getConstants());
        $this->assertArrayHasKey('FOOBAR', $info->getConstants());
        $this->assertEquals('FOOBAR', $info->getConstants()['FOOBAR']->getName());
        $this->assertEquals('baz', $info->getConstants()['FOOBAR']->getValue());
        $this->assertEquals('Public constant value', $info->getConstants()['FOOBAR']->getDescription());
        foreach ($info->getMethods() as $methodName => $methodInfo) {
            $this->assertClassInfoMethod(Foobar::class, $methodName, $methodInfo);
        }
        // The sequence is important!
        $this->assertEquals(
            ['__construct', 'anotherOne', 'baz', 'longParamDescriptions', 'yetAnotherOne', 'whoami'],
            array_keys($info->getMethods())
        );
        // Tests the examples
        $examples = $info->getExamples();
        $this->assertCount(3, $examples);
        $this->assertEquals('This is a PHP example:', $examples[0]['intro']);
        $this->assertEquals('php', $examples[0]['type']);
        $this->assertEquals('echo Foobar::whoami();', $examples[0]['code']);
        $this->assertEquals(
            'Something entirely different, but this is how to list files on linux:',
            $examples[1]['intro']
        );
        $this->assertEquals('', $examples[1]['type']);
        $this->assertEquals('ls -la .', $examples[1]['code']);
        $this->assertEquals("And another PHP example\nwith two lines of introduction.", $examples[2]['intro']);
        $this->assertEquals('php', $examples[2]['type']);
        $this->assertEquals('echo (new Foobar())->baz(\'foo\');', $examples[2]['code']);
    }

    /**
     * Tests the 'extends' and 'implements' statements
     *
     * @return void
     */
    public function testJsonFoobar(): void
    {
        $info = new ClassInfo(JsonFoobar::class);
        $this->assertEquals('JsonFoobar', $info->getShortName());
        $this->assertEquals(Foobar::class, $info->getExtends());
        $this->assertEquals([JsonSerializable::class], $info->getImplements());
        $this->assertCount(3, $info->getMethods());
        foreach ($info->getMethods() as $methodName => $methodInfo) {
            $this->assertClassInfoMethod(JsonFoobar::class, $methodName, $methodInfo);
        }
        // The sequence is important!
        $this->assertEquals(['jsonSerialize', '__toString', '__set_state'], array_keys($info->getMethods()));
        // No examples present
        $this->assertCount(0, $info->getExamples());
    }

    /**
     * Asserts all method data
     *
     * @param string          $className  Name of the class.
     * @param string          $methodName Name of the method.
     * @param ClassInfoMethod $methodInfo Data about the method.
     *
     * @return void
     */
    private function assertClassInfoMethod(string $className, string $methodName, ClassInfoMethod $methodInfo): void
    {
        $fullyQualifiedMethodName = $className . '::' . $methodName;
        switch ($fullyQualifiedMethodName) {
            case 'Tests\\TestSubject\\Foobar::anotherOne':
                $this->assertEquals('anotherOne', $methodInfo->getName());
                $this->assertEquals('Also just returns the input value', $methodInfo->getDescription());
                $this->assertFalse($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(1, $methodInfo->getParameters());
                $this->assertCount(2, $methodInfo->getReturnTypes());
                // assert parameter 'value'
                $this->assertArrayHasKey('$value', $methodInfo->getParameters());
                $this->assertEquals('value', $methodInfo->getParameters()['$value']->getName());
                $this->assertEquals('Input value.', $methodInfo->getParameters()['$value']->getDescription());
                $this->assertEquals('?string', $methodInfo->getParameters()['$value']->getType());
                $this->assertEquals('', $methodInfo->getParameters()['$value']->getDefaultValue());
                $this->assertFalse($methodInfo->getParameters()['$value']->isOptional());
                // assert return type 'string'
                $this->assertArrayHasKey('string', $methodInfo->getReturnTypes());
                $this->assertEquals('string', $methodInfo->getReturnTypes()['string']->getType());
                $this->assertEquals(
                    'A string is returned when a string is given.',
                    $methodInfo->getReturnTypes()['string']->getDescription()
                );
                $this->assertFalse($methodInfo->getReturnTypes()['string']->isNullable());
                // assert return type 'null'
                $this->assertArrayHasKey('null', $methodInfo->getReturnTypes());
                $this->assertEquals('null', $methodInfo->getReturnTypes()['null']->getType());
                $this->assertEquals(
                    'Null is returned when null is given.',
                    $methodInfo->getReturnTypes()['null']->getDescription()
                );
                $this->assertTrue($methodInfo->getReturnTypes()['null']->isNullable());
                break;
            case 'Tests\\TestSubject\\Foobar::yetAnotherOne':
                $this->assertEquals('yetAnotherOne', $methodInfo->getName());
                $this->assertEquals('Different notation', $methodInfo->getDescription());
                $this->assertFalse($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(0, $methodInfo->getParameters());
                $this->assertCount(1, $methodInfo->getReturnTypes());
                // assert return type 'string|null'
                $this->assertArrayHasKey('string|null', $methodInfo->getReturnTypes());
                $this->assertEquals('string', $methodInfo->getReturnTypes()['string|null']->getType());
                $this->assertEquals('', $methodInfo->getReturnTypes()['string|null']->getDescription());
                $this->assertTrue($methodInfo->getReturnTypes()['string|null']->isNullable());
                break;
            case 'Tests\\TestSubject\\Foobar::__construct':
                $this->assertEquals('__construct', $methodInfo->getName());
                $this->assertEquals('', $methodInfo->getDescription());
                $this->assertFalse($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(3, $methodInfo->getParameters());
                $this->assertCount(0, $methodInfo->getReturnTypes());
                // assert parameter 'foo'
                $this->assertArrayHasKey('$foo', $methodInfo->getParameters());
                $this->assertEquals('foo', $methodInfo->getParameters()['$foo']->getName());
                $this->assertEquals('', $methodInfo->getParameters()['$foo']->getDescription());
                $this->assertEquals('string', $methodInfo->getParameters()['$foo']->getType());
                $this->assertEquals('', $methodInfo->getParameters()['$foo']->getDefaultValue());
                $this->assertFalse($methodInfo->getParameters()['$foo']->isOptional());
                // assert parameter 'bar'
                $this->assertArrayHasKey('$bar', $methodInfo->getParameters());
                $this->assertEquals('bar', $methodInfo->getParameters()['$bar']->getName());
                $this->assertEquals('', $methodInfo->getParameters()['$bar']->getDescription());
                $this->assertEquals('int', $methodInfo->getParameters()['$bar']->getType());
                $this->assertEquals(1, $methodInfo->getParameters()['$bar']->getDefaultValue());
                $this->assertTrue($methodInfo->getParameters()['$bar']->isOptional());
                // assert parameter 'baz'
                $this->assertArrayHasKey('$baz', $methodInfo->getParameters());
                $this->assertEquals('baz', $methodInfo->getParameters()['$baz']->getName());
                $this->assertEquals('', $methodInfo->getParameters()['$baz']->getDescription());
                $this->assertEquals('mixed', $methodInfo->getParameters()['$baz']->getType());
                $this->assertEquals('foobar', $methodInfo->getParameters()['$baz']->getDefaultValue());
                $this->assertTrue($methodInfo->getParameters()['$baz']->isOptional());
                break;
            case 'Tests\\TestSubject\\Foobar::baz':
                // assert generic data
                $this->assertEquals('baz', $methodInfo->getName());
                $this->assertFalse($methodInfo->isStatic());
                $this->assertEquals('Returns the literal string back', $methodInfo->getDescription());
                $this->assertCount(1, $methodInfo->getThrows());
                $this->assertCount(1, $methodInfo->getParameters());
                $this->assertCount(1, $methodInfo->getReturnTypes());
                // assert parameter 'param1'
                $this->assertArrayHasKey('$param1', $methodInfo->getParameters());
                $this->assertEquals('param1', $methodInfo->getParameters()['$param1']->getName());
                $this->assertEquals('Input string.', $methodInfo->getParameters()['$param1']->getDescription());
                $this->assertEquals('string', $methodInfo->getParameters()['$param1']->getType());
                $this->assertEquals('', $methodInfo->getParameters()['$param1']->getDefaultValue());
                $this->assertFalse($methodInfo->getParameters()['$param1']->isOptional());
                // assert return type 'string'
                $this->assertArrayHasKey('string', $methodInfo->getReturnTypes());
                $this->assertEquals('string', $methodInfo->getReturnTypes()['string']->getType());
                $this->assertEquals('', $methodInfo->getReturnTypes()['string']->getDescription());
                $this->assertFalse($methodInfo->getReturnTypes()['string']->isNullable());
                // assert throw 'InvalidArgumentException'
                $this->assertArrayHasKey(InvalidArgumentException::class, $methodInfo->getThrows());
                $this->assertEquals(
                    InvalidArgumentException::class,
                    $methodInfo->getThrows()[InvalidArgumentException::class]->getThrowableFQCN()
                );
                $this->assertEquals(
                    'Thrown when the input string is empty.',
                    $methodInfo->getThrows()[InvalidArgumentException::class]->getDescription()
                );
                break;
            case 'Tests\TestSubject\Foobar::whoami':
                $this->assertEquals('whoami', $methodInfo->getName());
                $this->assertEquals('Returns the name of the called class', $methodInfo->getDescription());
                $this->assertTrue($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(0, $methodInfo->getParameters());
                $this->assertCount(1, $methodInfo->getReturnTypes());
                // assert return type 'string|null'
                $this->assertArrayHasKey('string|null', $methodInfo->getReturnTypes());
                $this->assertEquals('string', $methodInfo->getReturnTypes()['string|null']->getType());
                $this->assertEquals('', $methodInfo->getReturnTypes()['string|null']->getDescription());
                $this->assertTrue($methodInfo->getReturnTypes()['string|null']->isNullable());
                break;
            case 'Tests\\TestSubject\\JsonFoobar::jsonSerialize':
                $this->assertEquals('jsonSerialize', $methodInfo->getName());
                $this->assertEquals('', $methodInfo->getDescription());
                $this->assertFalse($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(0, $methodInfo->getParameters());
                $this->assertCount(0, $methodInfo->getReturnTypes());
                break;
            case 'Tests\\TestSubject\\JsonFoobar::__toString':
                $this->assertEquals('__toString', $methodInfo->getName());
                $this->assertEquals('Returns this object as string', $methodInfo->getDescription());
                $this->assertFalse($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(0, $methodInfo->getParameters());
                $this->assertCount(1, $methodInfo->getReturnTypes());
                // assert return type 'string'
                $this->assertArrayHasKey('string', $methodInfo->getReturnTypes());
                $this->assertEquals('string', $methodInfo->getReturnTypes()['string']->getType());
                $this->assertEquals('Always \'foobar\'.', $methodInfo->getReturnTypes()['string']->getDescription());
                $this->assertFalse($methodInfo->getReturnTypes()['string']->isNullable());
                break;
            case 'Tests\\TestSubject\\JsonFoobar::__set_state':
                $this->assertEquals('__set_state', $methodInfo->getName());
                $this->assertEquals('', $methodInfo->getDescription());
                $this->assertTrue($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(1, $methodInfo->getParameters());
                $this->assertCount(1, $methodInfo->getReturnTypes());
                // assert parameter 'anArray'
                $this->assertArrayHasKey('$anArray', $methodInfo->getParameters());
                $this->assertEquals('anArray', $methodInfo->getParameters()['$anArray']->getName());
                $this->assertEquals('', $methodInfo->getParameters()['$anArray']->getDescription());
                $this->assertEquals('array', $methodInfo->getParameters()['$anArray']->getType());
                $this->assertEquals('', $methodInfo->getParameters()['$anArray']->getDefaultValue());
                $this->assertFalse($methodInfo->getParameters()['$anArray']->isOptional());
                // assert return type 'self'
                $this->assertArrayHasKey('self', $methodInfo->getReturnTypes());
                $this->assertEquals('self', $methodInfo->getReturnTypes()['self']->getType());
                $this->assertEquals('', $methodInfo->getReturnTypes()['self']->getDescription());
                $this->assertFalse($methodInfo->getReturnTypes()['self']->isNullable());
                break;
            case 'Tests\\TestSubject\\Foobar::longParamDescriptions':
                $this->assertEquals('longParamDescriptions', $methodInfo->getName());
                $this->assertEquals(
                    "Sometimes a parameter can have a long description.\n \n "
                    . "Also, methods can have a longer description.",
                    $methodInfo->getDescription()
                );
                $this->assertFalse($methodInfo->isStatic());
                $this->assertCount(0, $methodInfo->getThrows());
                $this->assertCount(3, $methodInfo->getParameters());
                $this->assertCount(1, $methodInfo->getReturnTypes());
                // assert parameter 'param2'
                $this->assertArrayHasKey('$param2', $methodInfo->getParameters());
                $this->assertEquals('param2', $methodInfo->getParameters()['$param2']->getName());
                $this->assertEquals(
                    "This parameter has a longer description that is placed onto multiple lines which we also\n"
                    . "                       should support. Normally, whitespace is ignored, so for a next line we "
                    . "can use <br>\n                       So this content should end up on the next line.",
                    $methodInfo->getParameters()['$param2']->getDescription()
                );
                $this->assertEquals('string', $methodInfo->getParameters()['$param2']->getType());
                $this->assertEquals('', $methodInfo->getParameters()['$param2']->getDefaultValue());
                $this->assertFalse($methodInfo->getParameters()['$param2']->isOptional());
                break;
            default:
                $this->assertFalse(true, 'No assertions for method "' . $fullyQualifiedMethodName . '"');
        }
    }
}
