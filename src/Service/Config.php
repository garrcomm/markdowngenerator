<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\Service;

use InvalidArgumentException;
use RuntimeException;
use JsonException;

/**
 * Config object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class Config
{
    /**
     * Collection of all configuration values
     *
     * @see https://bitbucket.org/garrcomm/markdowngenerator/src/v1/schema/config-schema.json
     *
     * @var array{
     *   bootstrap: string,
     *   inputFiles: string[],
     *   outputDir: string
     * }
     */
    private array $configData;
    /**
     * List of classes that should be documented
     *
     * @var string[]
     */
    private array $objects = [];
    /**
     * Output path for all files
     *
     * @var string
     */
    private string $outputDir;

    /**
     * Reads and validates the config file.
     *
     * It's important to validate the full context of the configuration file, so all errors can be catched before
     * processing any data.
     *
     * @param string $configFile Path to the config file.
     *
     * @throws InvalidArgumentException Thrown when the config file can't be found.
     * @throws RuntimeException         Thrown when the config file can't be read or can't be interpreted.
     */
    public function __construct(string $configFile)
    {
        $this->loadConfiguration($configFile);
        $this->changeWorkingDirectory($configFile);
        $this->validateOutputDir();
        $this->executeBootstrap();
        $this->collectFiles();
    }

    /**
     * Validates the output directory
     *
     * @return void
     *
     * @throws InvalidArgumentException Thrown when the output directory value is not configured correctly.
     * @throws RuntimeException         Thrown when the directory can't be found.
     */
    private function validateOutputDir(): void
    {
        if (!isset($this->configData["outputDir"]) || !is_string($this->configData["outputDir"])) {
            throw new InvalidArgumentException('Config value "outputdir" is not a string');
        }

        $dir = realpath($this->configData['outputDir']);
        if ($dir === false || !is_dir($dir)) {
            throw new RuntimeException('Can\'t find the output directory: ' . $this->configData['outputDir']);
        }

        $this->outputDir = rtrim($dir, '\\/') . '/';
    }

    /**
     * Returns the output directory with an ending slash
     *
     * @return string
     */
    public function getOutputDirectory(): string
    {
        return $this->outputDir;
    }

    /**
     * Returns a list of all objects that should be indexed
     *
     * @return string[]
     */
    public function getObjects(): array
    {
        return $this->objects;
    }

    /**
     * Changes the working directory to the path in which the config file exists, so all relative paths work
     *
     * @param string $configFile Path to the config file.
     *
     * @return void
     *
     * @throws InvalidArgumentException Thrown when the config file can't be located properly.
     */
    private function changeWorkingDirectory(string $configFile): void
    {
        $realPath = realpath($configFile) ?: $configFile;
        chdir(pathinfo($realPath, PATHINFO_DIRNAME));
    }

    /**
     * Loads the configuration file and parses the json data
     *
     * @param string $configFile Path to the config file.
     *
     * @return void
     *
     * @throws InvalidArgumentException Thrown when the config file can't be found.
     * @throws RuntimeException         Thrown when the config file can't be read or can't be interpreted.
     */
    private function loadConfiguration(string $configFile): void
    {
        if (!file_exists($configFile)) {
            throw new InvalidArgumentException('Config file doesn\'t exist');
        }
        $jsonString = file_get_contents($configFile);
        if ($jsonString === false) {
            // @codeCoverageIgnoreStart
            // This scenario can't be tested
            throw new RuntimeException('Can\'t load configuration file');
            // @codeCoverageIgnoreEnd
        }
        try {
            $jsonArray = json_decode($jsonString, true, 255, JSON_THROW_ON_ERROR);
        } catch (JsonException $jsonException) {
            throw new RuntimeException('Can\'t parse config file: ' . $jsonException->getMessage(), 0, $jsonException);
        }
        if (!is_array($jsonArray)) {
            throw new RuntimeException('Config file should contain a json array');
        }
        $this->configData = $jsonArray;
    }

    /**
     * Loads the bootstrap file
     *
     * @return void
     *
     * @throws RuntimeException Thrown when the bootstrap file can't be read or can't be interpreted.
     */
    private function executeBootstrap(): void
    {
        if (!isset($this->configData['bootstrap'])) {
            return;
        }
        if (!file_exists($this->configData['bootstrap'])) {
            throw new RuntimeException('Bootstrap file doesn\'t exist');
        }
        try {
            include_once $this->configData['bootstrap'];
        } catch (\Throwable $throwable) {
            throw new RuntimeException('Can\'t load the bootstrap file: ' . $throwable->getMessage(), 0, $throwable);
        }
    }

    /**
     * Reads the input files and collects all class names
     *
     * @return void
     *
     * @throws RuntimeException         Thrown when a class file can't be read or can't be interpreted.
     * @throws InvalidArgumentException Thrown when no classes are found.
     */
    private function collectFiles(): void
    {
        if (!isset($this->configData['inputFiles']) || !is_array($this->configData['inputFiles'])) {
            throw new InvalidArgumentException('No inputfiles specified');
        }

        foreach ($this->configData['inputFiles'] as $inputFile) {
            $list = glob($inputFile);
            if ($list === false) {
                // @codeCoverageIgnoreStart
                // On some systems it is impossible to distinguish between empty match and an error.
                throw new RuntimeException('Incorrect input file match: "' . $inputFile . '"');
                // @codeCoverageIgnoreEnd
            }
            foreach ($list as $file) {
                if (is_dir($file) || !file_exists($file) || !is_readable($file)) {
                    // @codeCoverageIgnoreStart
                    // This should not happen, but due to filesystem issues it's not entirely impossible
                    throw new RuntimeException('Can\'t read file: "' . $inputFile . '"');
                    // @codeCoverageIgnoreEnd
                }
                try {
                    include_once $file;
                } catch (\Throwable $throwable) {
                    throw new RuntimeException(
                        'Can\'t load class file "' . $file . '": ' . $throwable->getMessage(),
                        0,
                        $throwable
                    );
                }
                $this->objects = array_merge($this->objects, $this->getClassesInFile($file));
            }
        }
        if (count($this->objects) === 0) {
            throw new InvalidArgumentException('No class files found');
        }
    }

    /**
     * Returns the base namespace for documented objects
     *
     * @return string
     */
    public function getBaseNamespace(): string
    {
        return rtrim($this->configData['baseNamespace'] ?? '', '\\') . '\\';
    }

    /**
     * Returns the path to the summary file
     *
     * @param boolean $absolutePath Set to true to get the absolute path.
     *
     * @return string|null
     */
    public function getSummaryFile(bool $absolutePath = false): ?string
    {
        if (empty($this->configData['summaryFile'])) {
            return null;
        }
        $file = $this->configData['summaryFile'];
        return $absolutePath ? trim(`realpath $file`) : $file;
    }

    /**
     * Returns the relative output path from the summary file (in case a summary file exists)
     *
     * @return string|null
     */
    public function getOutputRelativeToSummary(): ?string
    {
        if (empty($this->configData['summaryFile'])) {
            return null;
        }
        $summaryFile = $this->getSummaryFile(true) ?? '';
        $outputDirectory = $this->getOutputDirectory();

        // Paths are the same
        if (pathinfo($summaryFile, PATHINFO_DIRNAME) == rtrim($outputDirectory, '/')) {
            return '';
        }

        // Splits paths and try to get as close as possible
        $summaryFile = explode('/', $summaryFile);
        $outputDirectory = explode('/', $outputDirectory);
        for ($i = 0; $i < count($summaryFile); ++$i) {
            if ($summaryFile[$i] != $outputDirectory[$i]) {
                break;
            }
            $summaryFile[$i] = null;
            $outputDirectory[$i] = null;
        }
        $summaryFile = array_filter($summaryFile);
        $outputDirectory = array_filter($outputDirectory);

        return rtrim(str_repeat('../', count($summaryFile) - 1) . implode('/', $outputDirectory), '/') . '/';
    }

    /**
     * Reads a PHP file and returns all available classes in the file
     *
     * @param string $filename Path to a PHP file.
     *
     * @return string[]
     */
    private function getClassesInFile(string $filename): array
    {
        $classNames = array();

        $data = file_get_contents($filename);
        if ($data === false) {
            // @codeCoverageIgnoreStart
            // This can't really happen since we already included the file as well, but better safe than sorry
            throw new RuntimeException('Can\'t read file: ' . $filename);
            // @codeCoverageIgnoreEnd
        }
        $tokens = token_get_all($data);
        $registerNamespace = false;
        $registerClass = false;
        $classDeclaration = '';
        $namespace = '';
        for ($iterator = 0; $iterator < count($tokens); ++$iterator) {
            if (is_string($tokens[$iterator])) {
                if (in_array($tokens[$iterator], ['{', ';'])) {
                    if ($registerClass) {
                        $whitespaceSplit = preg_split('/\s/', trim($classDeclaration), 2);
                        $classNames[$iterator] = trim($namespace) . '\\' . ($whitespaceSplit[0] ?? $classDeclaration);
                    }
                    $registerClass = false;
                    $registerNamespace = false;
                }
            } elseif ($tokens[$iterator][0] === T_NAMESPACE) {
                $registerNamespace = true;
                $namespace = '';
            } elseif ($tokens[$iterator][0] === T_CLASS) {
                if ($iterator > 0 && $tokens[$iterator - 1][0] !== T_DOUBLE_COLON) {
                    $registerClass = true;
                }
            } elseif ($registerClass) {
                $classDeclaration .= $tokens[$iterator][1];
            } elseif ($registerNamespace) {
                $namespace .= $tokens[$iterator][1];
            }
        }

        return $classNames;
    }
}
