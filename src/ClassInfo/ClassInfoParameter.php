<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\ClassInfo;

/**
 * ClassInfoParameter object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoParameter
{
    /**
     * Name of the parameter
     *
     * @var string
     */
    private string $name;
    /**
     * Type of the parameter
     *
     * @var string
     */
    private string $type;
    /**
     * Is the parameter optional?
     *
     * @var bool
     */
    private bool $optional;
    /**
     * Default value of the parameter
     *
     * @var mixed
     */
    private $defaultValue;
    /**
     * Description of the parameter
     *
     * @var string
     */
    private string $description = '';

    /**
     * Constructs a new parameter definition
     *
     * @param string  $name         Name of the parameter.
     * @param string  $type         Type of the parameter.
     * @param boolean $optional     True when the parameter is optional.
     * @param mixed   $defaultValue Default value of the parameter.
     */
    public function __construct(string $name, string $type, bool $optional, $defaultValue)
    {
        $this->name = $name;
        $this->type = $type;
        $this->optional = $optional;
        $this->defaultValue = $defaultValue;
    }

    /**
     * Sets the type of the parameter
     *
     * @param string $type Type of the parameter.
     *
     * @return void
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * Sets the description of the parameter
     *
     * @param string $description The description of the parameter.
     *
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * Returns the name of the parameter
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the type of the parameter
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Returns whether the parameter is optional or not
     *
     * @return boolean
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * Returns the default value
     *
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Returns the description of the parameter
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
