## Class synopsis

```php
class JsonFoobar extends Tests\TestSubject\Foobar implements JsonSerializable {

    /* Constants */
    public const FOOBAR = 'baz';

    /* Methods */
    public jsonSerialize()
    public __toString(): string
    public static __set_state(array anArray): self
}
```

<a id="constants"></a>
## Constants

**JsonFoobar::FOOBAR**  
&nbsp;&nbsp;&nbsp;&nbsp;Public constant value


<a id="methods"></a>
## Methods

* [JsonFoobar::jsonSerialize](#jsonfoobar--jsonserialize) — 
* [JsonFoobar::__toString](#jsonfoobar----tostring) — Returns this object as string
* [JsonFoobar::__set_state](#jsonfoobar----set-state) — 


<a id="jsonfoobar--jsonserialize"></a>
## JsonFoobar::jsonSerialize
#### Description

```php
public JsonFoobar::jsonSerialize()
```



***
<a id="jsonfoobar----tostring"></a>
## JsonFoobar::__toString
#### Description

```php
public JsonFoobar::__toString(): string
```

Returns this object as string

#### Return Values

Always 'foobar'.

***
<a id="jsonfoobar----set-state"></a>
## JsonFoobar::__set_state
#### Description

```php
public static JsonFoobar::__set_state(array anArray): self
```



#### Parameters

* **anArray**  


#### Return Values



