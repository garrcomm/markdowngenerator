<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\ClassInfo;

/**
 * ClassInfoReturnType object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfoReturnType
{
    /**
     * The return type(s) separated by pipes
     *
     * @var string
     */
    private string $type;
    /**
     * Description of the returned value
     *
     * @var string
     */
    private string $description;
    /**
     * Is the return type nullable?
     *
     * @var bool
     */
    private bool $nullable = false;

    /**
     * Constructs a new return type definition
     *
     * @param string $typeList    The return type(s) separated by pipes.
     * @param string $description Description of the returned value.
     */
    public function __construct(string $typeList, string $description)
    {
        if (substr($typeList, 0, 1) == '?') {
            $typeList = substr($typeList, 1) . '|null';
        }

        // Remove null and sort list
        $types = [];
        foreach (explode('|', $typeList) as $type) {
            if (strtolower($type) === 'null') {
                $this->nullable = true;
                continue;
            }
            $types[] = $type;
        }
        sort($types);

        $this->type = implode('|', $types);
        $this->description = $description;
        if ($this->type === '') {
            $this->type = 'null';
        }
    }

    /**
     * Returns the type(s) pipe separated
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Returns the description of the return value
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Returns whether the return value can be null
     *
     * @return boolean
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }
}
