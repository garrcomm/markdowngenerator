<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\ClassInfo;

use ReflectionClass;
use InvalidArgumentException;

/**
 * ClassInfo object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class ClassInfo
{
    /**
     * Reference to the reflection class
     *
     * @var ReflectionClass
     */
    private ReflectionClass $reflectionClass;

    /**
     * Constructs a new ClassInfo object based on a specific class.
     *
     * @param string $classFQN Fully qualified class name.
     *
     * @throws InvalidArgumentException Thrown when the class can't be found.
     */
    public function __construct(string $classFQN)
    {
        if (!class_exists($classFQN)) {
            throw new InvalidArgumentException('Not a valid FQCN: ' . $classFQN);
        }
        $this->reflectionClass = new ReflectionClass($classFQN);
    }

    /**
     * Returns the short name of the class
     *
     * @return string
     */
    public function getShortName(): string
    {
        return $this->reflectionClass->getShortName();
    }

    /**
     * Gets the name of the extension which defined the class (or null when there is none)
     *
     * @return string|null
     */
    public function getExtends(): ?string
    {
        $result = $this->reflectionClass->getParentClass();
        return $result === false ? null : $result->getName();
    }

    /**
     * Returns a list of classes implemented by this class.
     *
     * @return string[]
     */
    public function getImplements(): array
    {
        return $this->reflectionClass->getInterfaceNames();
    }

    /**
     * Returns a list of code examples
     *
     * @return array{intro: string, type: string, code: string}[]
     */
    public function getExamples(): array
    {
        return $this->getExamplesFromDocblock($this->reflectionClass->getDocComment() ?: '');
    }

    /**
     * Parses all code examples from a docblock
     *
     * @param string $docblock The docblock.
     *
     * @return array{intro: string, type: string, code: string}[]
     */
    private function getExamplesFromDocblock(string $docblock): array
    {
        if (trim($docblock) === '') {
            return [];
        }
        /**
         * According to https://pear.php.net/manual/en/standards.sample.php there are two type of examples:
         *
         * PHP examples:
         * <code>
         *   php_code_example();
         * </code>
         *
         * Non-php examples:
         * <samp>
         *   Something else here;
         * </samp>
         *
         * The first line above the example is descriptive.
         */
        preg_match_all(
            '/(?P<intro>.*?)\n\s*<(?P<type>code|samp)>(?P<code>.*?)<\/(code|samp)>/is',
            "\n\n" . $this->ltrimBlock($this->removeDocblockComments($docblock)),
            $exampleMatches,
            PREG_SET_ORDER
        );

        $examples = array();
        foreach ($exampleMatches as $exampleMatch) {
            $introParts = preg_split('/(\r\n\r\n|\n\n)/', $exampleMatch['intro']);
            if (!is_array($introParts)) {
                // @codeCoverageIgnoreStart
                continue;
                // @codeCoverageIgnoreEnd
            }
            $examples[] = [
                'intro' => trim($this->ltrimBlock((string)end($introParts))),
                'type' => strtolower($exampleMatch['type']) === 'code' ? 'php' : '',
                'code' => $this->ltrimBlock($exampleMatch['code']),
            ];
        }

        return $examples;
    }

    /**
     * Returns details about all public class constants
     *
     * @return array<string, ClassInfoConstant>
     */
    public function getConstants(): array
    {
        $constants = array();

        foreach ($this->reflectionClass->getReflectionConstants() as $reflectionConstant) {
            if (!$reflectionConstant->isPublic()) {
                continue;
            }
            $constants[$reflectionConstant->getName()] = new ClassInfoConstant(
                $reflectionConstant->getName(),
                $reflectionConstant->getValue(),
                $this->removeDocblockComments($reflectionConstant->getDocComment() ?: ''),
            );
        }
        ksort($constants);

        return $constants;
    }

    /**
     * Returns details about all public methods in this class
     *
     * @return array<string, ClassInfoMethod>
     */
    public function getMethods(): array
    {
        $construct = [];
        $methods = [];
        $magicMethods = [];
        $staticMethods = [];
        $staticMagicMethods = [];

        foreach ($this->reflectionClass->getMethods() as $method) {
            if ($method->getDeclaringClass()->getName() !== $this->reflectionClass->getName()) {
                continue;
            }
            if (!$method->isPublic()) {
                continue;
            }
            $docblock = $this->removeDocblockComments($method->getDocComment() ?: '');
            if ($method->getName() == '__construct') {
                $construct[$method->getName()] = new ClassInfoMethod($method, $docblock);
            } elseif ($method->isStatic() && substr($method->getName(), 0, 2) == '__') {
                $staticMagicMethods[$method->getName()] = new ClassInfoMethod($method, $docblock);
            } elseif ($method->isStatic()) {
                $staticMethods[$method->getName()] = new ClassInfoMethod($method, $docblock);
            } elseif (substr($method->getName(), 0, 2) == '__') {
                $magicMethods[$method->getName()] = new ClassInfoMethod($method, $docblock);
            } else {
                $methods[$method->getName()] = new ClassInfoMethod($method, $docblock);
            }
        }

        // Methods are sorted by name, grouped by non-static / static / magic, and constructor first
        ksort($methods);
        ksort($magicMethods);
        ksort($staticMethods);
        ksort($staticMagicMethods);
        return $construct + $methods + $magicMethods + $staticMethods + $staticMagicMethods;
    }

    /**
     * Removes comment characters in a docblock
     *
     * @param string $docBlock The docblock.
     *
     * @return string
     */
    private function removeDocblockComments(string $docBlock): string
    {
        return trim(preg_replace('/(^[\s]{0,}\*(\/|)|^[\s]{0,}\/\*\*)/m', '', $docBlock) ?? '');
    }

    /**
     * Removes all leading whitespaces in a block with multiple rows
     *
     * @param string $input Input string.
     *
     * @return string
     */
    private function ltrimBlock(string $input): string
    {
        $lines = explode("\n", $input);

        // Looks up the minimum amount of whitespace in front of each line
        $whitespaceMinimum = 999;
        foreach ($lines as $line) {
            if (trim($line) == '') {
                continue;
            }
            $whitespace = strlen($line) - strlen(trim($line));
            if ($whitespace < $whitespaceMinimum) {
                $whitespaceMinimum = $whitespace;
            }
        }

        // Trims the lines according to the less amount of whitespace
        $return = '';
        foreach ($lines as $line) {
            $return .= substr($line, $whitespaceMinimum) . "\n";
        }

        return trim($return);
    }
}
