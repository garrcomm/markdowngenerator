<?php

declare(strict_types=1);

namespace Tests\TestSubject;

use JsonSerializable;

class JsonFoobar extends FooBar implements JsonSerializable
{
    public static function __set_state(array $anArray): self
    {
        return new JsonFoobar('', 1, '');
    }

    public function jsonSerialize()
    {
        return ['foo' => 'bar'];
    }

    /**
     * Returns this object as string
     *
     * @return string Always 'foobar'.
     */
    public function __toString()
    {
        return 'foobar';
    }
}

