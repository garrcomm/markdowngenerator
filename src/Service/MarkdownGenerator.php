<?php

declare(strict_types=1);

namespace Garrcomm\MarkdownGenerator\Service;

use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfo;
use Garrcomm\MarkdownGenerator\ClassInfo\ClassInfoMethod;

/**
 * MarkdownGenerator object
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/markdowngenerator
 */
class MarkdownGenerator
{
    /**
     * Reference to the class information
     *
     * @var ClassInfo
     */
    private ClassInfo $classInfo;

    /**
     * Reads out class information and formats it in markdown
     *
     * @param ClassInfo $classInfo Reference to the class information.
     */
    public function __construct(ClassInfo $classInfo)
    {
        $this->classInfo = $classInfo;
    }

    /**
     * Returns the class examples paragraph as string
     *
     * @param integer $header The header type for the Examples header.
     *
     * @return string
     */
    public function getClassExamples(int $header = 2): string
    {
        $examples = $this->classInfo->getExamples();
        if (count($examples) === 0) {
            return '';
        }
        $return = str_repeat('#', $header) . ' Class example' . (count($examples) == 1 ? '' : 's') . PHP_EOL;
        foreach ($examples as $example) {
            $return .= PHP_EOL;
            $return .= $example['intro'] ? $example['intro'] . PHP_EOL . PHP_EOL : '';
            $return .= "```" . $example['type'] . PHP_EOL;
            $return .= $example['code'] . PHP_EOL;
            $return .= "```" . PHP_EOL;
        }
        $return .= PHP_EOL;

        return $return;
    }

    /**
     * Returns the class synopsis as markdown string
     *
     * @param integer $header The header type for the Class synopsis header.
     *
     * @return string
     */
    public function getClassSynopsis(int $header = 2): string
    {
        $return = str_repeat('#', $header) . ' Class synopsis' . PHP_EOL;
        $return .= PHP_EOL;
        $return .= '```php' . PHP_EOL;
        $return .= $this->getClassDeclaration() . ' {' . PHP_EOL;
        $return .= PHP_EOL;
        if (!empty($this->classInfo->getConstants())) {
            $return .= '    /* Constants */' . PHP_EOL;
            foreach ($this->classInfo->getConstants() as $constant) {
                $return .= '    public const ' . $constant->getName() . ' = '
                    . var_export($constant->getValue(), true) . ';' . PHP_EOL;
            }
            $return .= PHP_EOL;
        }
        if (!empty($this->classInfo->getMethods())) {
            $return .= '    /* Methods */' . PHP_EOL;
            foreach ($this->classInfo->getMethods() as $method) {
                $return .= '    ' . $this->getMethodDeclaration($method) . PHP_EOL;
            }
            $return .= PHP_EOL;
        }
        $return = rtrim($return) . PHP_EOL . '}' . PHP_EOL;
        $return .= '```' . PHP_EOL;

        return $return;
    }

    /**
     * Returns a list of constants in markdown format
     *
     * @param integer $header The header type for the Constants header.
     *
     * @return string
     */
    public function getConstants(int $header = 2): string
    {
        $className = $this->classInfo->getShortName();
        $constants = $this->classInfo->getConstants();

        if (empty($constants)) {
            return '';
        }
        $return = PHP_EOL . '<a id="constants"></a>' . PHP_EOL . str_repeat('#', $header) . ' Constants' . PHP_EOL;
        $return .= PHP_EOL;
        foreach ($constants as $constant) {
            $return .= '**' . $className . '::' . $constant->getName() . '**  ' . PHP_EOL;
            $return .= '&nbsp;&nbsp;&nbsp;&nbsp;' . $constant->getDescription() . PHP_EOL;
            $return .= PHP_EOL;
        }
        return $return;
    }

    /**
     * Returns a list of methods in markdown format
     *
     * @param string  $linkPrefix Prefix for hyperlinks to method details.
     * @param integer $header     The header type for the Methods header.
     *
     * @return string
     */
    public function getMethodList(string $linkPrefix = '#', int $header = 2): string
    {
        $className = $this->classInfo->getShortName();
        $methods = $this->classInfo->getMethods();
        if (empty($methods)) {
            return '';
        }
        $return = PHP_EOL . '<a id="methods"></a>' . PHP_EOL . str_repeat('#', $header) . ' Methods' . PHP_EOL;
        $return .= PHP_EOL;
        foreach ($methods as $method) {
            $fullName = $className . '::' . $method->getName();
            $description = ltrim($this->fixIndents($method->getDescription(), '    '));
            if (strpos($description, "\n") !== false) {
                $description .= PHP_EOL . '    ';
            }
            $return .= '* [' . $fullName . '](' . $linkPrefix . $this->getAnchorName($fullName) . ') — '
                . $description . PHP_EOL;
        }
        $return .= PHP_EOL;
        return $return;
    }

    /**
     * Returns details about all methods in markdown format
     *
     * @return string
     */
    public function getMethodDetails(): string
    {
        $className = $this->classInfo->getShortName();
        $methods = $this->classInfo->getMethods();

        if (count($methods) === 0) {
            return '';
        }

        $return = PHP_EOL;

        foreach ($methods as $method) {
            $fullName = $className . '::' . $method->getName();
            $return .= '<a id="' . $this->getAnchorName($fullName) . '"></a>' . PHP_EOL ;
            $return .= '## ' . $fullName . PHP_EOL;
            $return .= '#### Description' . PHP_EOL . PHP_EOL;
            $return .= '```php' . PHP_EOL;
            $return .= $this->getMethodDeclaration($method, $className . '::') . PHP_EOL;
            $return .= '```' . PHP_EOL . PHP_EOL;
            $return .= $method->getDescription() . PHP_EOL;
            if (!empty($method->getParameters())) {
                $return .= PHP_EOL . '#### Parameters' . PHP_EOL . PHP_EOL;
                foreach ($method->getParameters() as $parameter) {
                    $return .= '* **' . ltrim($parameter->getName(), '$') . '**  ' . PHP_EOL;
                    $return .= $this->fixIndents($parameter->getDescription(), '    ', '  ') . PHP_EOL;
                }
            }
            if (!empty($method->getReturnTypes())) {
                $return .= PHP_EOL . '#### Return Values' . PHP_EOL . PHP_EOL;
                foreach ($method->getReturnTypes() as $returnType) {
                    $return .= $this->fixIndents($returnType->getDescription(), '', '  ') . PHP_EOL;
                }
            }
            if (!empty($method->getThrows())) {
                $return .= PHP_EOL . '#### Errors/Exceptions' . PHP_EOL . PHP_EOL;
                foreach ($method->getThrows() as $throwable) {
                    $return .= $this->fixIndents($throwable->getDescription(), '', '  ') . PHP_EOL;
                }
            }

            $return .= PHP_EOL . '***' . PHP_EOL;
        }

        return substr($return, 0, -4);
    }

    /**
     * Returns the class declaration (`class {name} extends {name} implements {name}, {name}`)
     *
     * @return string
     */
    private function getClassDeclaration(): string
    {
        $classDeclaration = 'class ' . $this->classInfo->getShortName();
        if ($this->classInfo->getExtends()) {
            $classDeclaration .= ' extends ' . $this->classInfo->getExtends();
        }
        if (!empty($this->classInfo->getImplements())) {
            $classDeclaration .= ' implements ' . implode(', ', $this->classInfo->getImplements());
        }
        return $classDeclaration;
    }

    /**
     * Returns the method declaration (`public {static?} {$methodPrefix}{name}({...parameters}): {returntype}`)
     *
     * @param ClassInfoMethod $method       Method information.
     * @param string          $methodPrefix Prefix in the method declaration.
     *
     * @return string
     */
    private function getMethodDeclaration(ClassInfoMethod $method, string $methodPrefix = ''): string
    {
        $return = 'public ' . ($method->isStatic() ? 'static ' : '') . $methodPrefix . $method->getName() . '(';
        foreach (array_values($method->getParameters()) as $iterator => $parameter) {
            if ($iterator > 0) {
                $return .= ', ';
            }
            $return .= $parameter->getType() . ' ' . $parameter->getName();
            if ($parameter->isOptional()) {
                $return .= ' = ' .
                    ($parameter->getDefaultValue() === null ? 'null' : var_export($parameter->getDefaultValue(), true));
            }
        }
        $return .= ')';
        if (!empty($method->getReturnTypes())) {
            $returnType = array_values($method->getReturnTypes())[0]->getType();
            $return .= ': ' . $returnType;
        }

        return $return;
    }

    /**
     * Converts a name to a safe name for markdown anchors
     *
     * @param string $fullName The full name.
     *
     * @return string
     */
    public function getAnchorName(string $fullName): string
    {
        return strtolower(preg_replace('/[^a-z0-9]/i', '-', str_replace('\\', '', $fullName)) ?: '');
    }

    /**
     * Fix whitespace indents
     *
     * @param string $text      Original text.
     * @param string $indention Required indention.
     * @param string $suffix    Optionally a suffix (`  ` would result in improved new line handling in markdown).
     *
     * @return string
     */
    private function fixIndents(string $text, string $indention, string $suffix = ''): string
    {
        $return = '';
        $parts = explode(PHP_EOL, $text);
        foreach ($parts as $part) {
            $return .= $indention . ltrim($part) . $suffix . PHP_EOL;
        }
        return rtrim($return);
    }
}
